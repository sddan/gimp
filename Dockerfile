# Dockerfile to create an image with GIMP 2.10.18 to run python-fu scripts
#
# Author: Samuel Dan
# Contact: samuel.d.dan@gmail.com

# Base image
FROM ubuntu:20.04 as BUILDER
LABEL distro="Ubuntu 20.04"

##################################################
# Build GIMP 2.10.18 from source
##################################################

#COPY gimp-build-dependencies.txt .
#RUN xargs -a gimp-build-dependencies.txt sudo apt-get install -y
#RUN wget https://download.gimp.org/mirror/pub/gimp/v2.10/gimp-2.10.18.tar.bz2
#RUN bzip2 -d gimp-2.10.18.tar.bz2
#RUN tar xvfs gimp-2.10.18.tar
#WORKDIR gimp-2.10.18
#RUN ./configure
#RUN make
#RUN make install

# Set timezone so that installation does not request user input
RUN ln -fs /usr/share/zoneinfo/Asia/Kuala_Lumpur /etc/localtime
RUN export DEBIAN_FRONTEND=noninteractive

##################################################
# Install GIMP 2.10.18 from apt repository
##################################################

RUN apt-get update && apt-get install gimp wget python python-cairo python-gobject-2 -y

# Install python2 for GIMP scripting. Manual install as py27 is deprecated in Ubuntu 18.04 onward.
#RUN apt install vim sudo wget python python-cairo python-gobject-2 -y

# The following lines are commented out as the build environment contains those files, and will
# be copied in via the resources folder above. Ubuntu images do not have wget installed
RUN wget http://archive.ubuntu.com/ubuntu/pool/universe/p/pygtk/python-gtk2_2.24.0-6_amd64.deb
RUN wget http://archive.ubuntu.com/ubuntu/pool/universe/g/gimp/gimp-python_2.10.8-2_amd64.deb
RUN dpkg -i python-gtk2_2.24.0-6_amd64.deb
RUN dpkg -i gimp-python_2.10.8-2_amd64.deb
RUN apt install python2

# Copy the following directories from BUILDER image
# etc var usr
FROM ubuntu:20.04
WORKDIR /
COPY --from=BUILDER /etc ./etc
COPY --from=BUILDER /usr ./usr
#COPY --from=BUILDER /var ./var
